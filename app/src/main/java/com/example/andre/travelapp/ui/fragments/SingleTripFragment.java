package com.example.andre.travelapp.ui.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.example.andre.travelapp.R;
import com.example.andre.travelapp.constants.ActivityConstants;
import com.example.andre.travelapp.constants.DatabaseConstants;
import com.example.andre.travelapp.models.LocationModel;
import com.example.andre.travelapp.models.Trip;
import com.example.andre.travelapp.models.User;
import com.example.andre.travelapp.ui.activities.MainActivity;
import com.example.andre.travelapp.utils.MarkerUtils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import static androidx.constraintlayout.motion.widget.MotionScene.TAG;

public class SingleTripFragment extends Fragment implements OnMapReadyCallback {

    private View view;

    private ViewFlipper viewFlipper;

    private TextView titleTextView;
    private TextView startDateTextView;
    private TextView endDateTextView;
    private TextView numberOfPeopleTextView;
    private GoogleMap mapCurrent;
    private MapView map;
    private Button chatBtn, deleteBtn, groupMembersBtn;

    private Trip currentTrip;
    private MarkerUtils markerUtils;
    private List<Bitmap> imageList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_single_trip, container, false);
        onInit();
        setInfo();
        chatBtn.setOnClickListener(v -> openTripChat());
        deleteBtn.setOnClickListener(v -> deleteTrip());
        groupMembersBtn.setOnClickListener(v -> {
            openGroupMembers();
        });
        return view;
    }

    private void onInit() {
        viewFlipper = view.findViewById(R.id.image_view);
        titleTextView = view.findViewById(R.id.list_title);
        startDateTextView = view.findViewById(R.id.list_startDate);
        endDateTextView = view.findViewById(R.id.list_endDate);
        numberOfPeopleTextView = view.findViewById(R.id.list_numberOfPeople);
        chatBtn = view.findViewById(R.id.chat_btn);
        deleteBtn = view.findViewById(R.id.delete_btn);
        groupMembersBtn = view.findViewById(R.id.group_members_btn);
        map = view.findViewById(R.id.list_map);
        if (map != null) {
            map.onCreate(null);
            map.onResume();
            map.getMapAsync(this);
        }
        markerUtils = new MarkerUtils();
    }

    private void setInfo() {
        Bundle bundle = this.getArguments();
        if (bundle != null && !bundle.isEmpty()) {
            Log.d(TAG, "BUNDLE: " + bundle);
            currentTrip = bundle.getParcelable("trip");
            displayTripDetails();

            imageList = bundle.getParcelableArrayList("imageList");
            displayImageCaroussel();

            String currentUserUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            if (currentTrip.getUserAdminUid().equals(currentUserUid)) {
                deleteBtn.setVisibility(View.VISIBLE);
            } else {
                deleteBtn.setText("LEAVE TRIP");
            }
            bundle.clear();
        } else {
            displayImageCaroussel();
            displayTripDetails();
        }
    }

    private void displayTripDetails() {
        titleTextView.setText(currentTrip.getTitle());
        startDateTextView.setText("Departure: " + currentTrip.getStartDate());
        endDateTextView.setText("Arrival: " + currentTrip.getEndDate());
        numberOfPeopleTextView.setText(currentTrip.getTripDescription());
    }

    private void displayImageCaroussel() {
        if (imageList.size() > 1) {
            for (Bitmap img : imageList) {
                flipperImages(img);
            }
        } else {
            ImageView imageView = new ImageView(getContext());
            imageView.setImageBitmap(imageList.get(0));
            viewFlipper.addView(imageView);
        }
    }

    private void flipperImages(Bitmap img) {
        ImageView imageView = new ImageView(getContext());
        imageView.setImageBitmap(img);
        viewFlipper.addView(imageView);
        viewFlipper.setFlipInterval(2500);
        viewFlipper.setAutoStart(true);
        viewFlipper.setInAnimation(getContext(), android.R.anim.fade_in);
        viewFlipper.setOutAnimation(getContext(), android.R.anim.fade_out);
    }

    private void openTripChat() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("trip", currentTrip);
        ChatFragment chatFragment = new ChatFragment();
        chatFragment.setArguments(bundle);
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, chatFragment)
                .addToBackStack(null).commit();
    }

    private void deleteTrip() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        if (deleteBtn.getText().equals("CANCEL TRIP")) {
            alertDialog.setMessage("Are you sure you want to cancel this trip?");
            alertDialog.setPositiveButton("Yes", deleteTripListener());
        } else if (deleteBtn.getText().equals("LEAVE TRIP")) {
            alertDialog.setMessage("Are you sure you want to leave this trip?");
            alertDialog.setPositiveButton("Yes", leaveTripListener());
        }
        alertDialog.setNegativeButton("No", closeAlertDialog());
        alertDialog.show();
    }

    private DialogInterface.OnClickListener deleteTripListener() {
        return (dialog, which) -> {
            FirebaseDatabase.getInstance().getReference().child(DatabaseConstants.TRIPS_DATABASE)
                    .child(currentTrip.getTripId()).removeValue();
            FirebaseDatabase.getInstance().getReference().child(DatabaseConstants.CHATS_DATABASE)
                    .child(currentTrip.getTripId()).removeValue();

            DatabaseReference userRef = FirebaseDatabase.getInstance().getReference()
                    .child(DatabaseConstants.USERS_DATABASE);
            userRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        String key = snapshot.getKey();
                        User user = snapshot.getValue(User.class);
                        List<String> userTripsList = (ArrayList<String>) snapshot
                                .child(DatabaseConstants.USERS_MY_TRIPS).getValue();
                        Log.d(TAG, "deleteTripListener: " + user.getUsername());
                        if (userTripsList != null && userTripsList.contains(currentTrip.getTripId())) {
                            Log.d(TAG, "deleteTripListener: ");
                            userTripsList.remove(currentTrip.getTripId());
                            user.setMyTripsUid(userTripsList);
                            userRef.child(key).setValue(user);
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            Intent intent = new Intent(getActivity(), MainActivity.class);
            intent.putExtra(ActivityConstants.PREVIOUS_ACTIVITY_KEY, ActivityConstants.SINGLE_TRIP_FRAGMENT);
            startActivity(intent);
        };
    }

    private DialogInterface.OnClickListener closeAlertDialog() {
        return (dialog, which) -> {
        };
    }

    private DialogInterface.OnClickListener leaveTripListener() {
        return (dialog, which) -> {
            String currentUserUid = FirebaseAuth.getInstance().getCurrentUser().getUid();

            DatabaseReference userRef = FirebaseDatabase.getInstance().getReference()
                    .child(DatabaseConstants.USERS_DATABASE).child(currentUserUid);
            DatabaseReference tripRef = FirebaseDatabase.getInstance().getReference()
                    .child(DatabaseConstants.TRIPS_DATABASE).child(currentTrip.getTripId());
            DatabaseReference chatRef = FirebaseDatabase.getInstance().getReference()
                    .child(DatabaseConstants.CHATS_DATABASE).child(currentTrip.getTripId());

            userRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    List<String> tripsList = user.getMyTripsUid();
                    if (tripsList != null) {
                        tripsList.remove(currentTrip.getTripId());
                    }
                    userRef.child(DatabaseConstants.USERS_MY_TRIPS).setValue(tripsList);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            tripRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Trip trip = dataSnapshot.getValue(Trip.class);
                    List<String> attendingUsersList = trip.getAttendingUsersUids();
                    if (attendingUsersList != null) {
                        attendingUsersList.remove(currentUserUid);
                    }
                    tripRef.child(DatabaseConstants.TRIP_USERS_LIST).setValue(attendingUsersList);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            chatRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    List<String> attendingUsersList = (List<String>) dataSnapshot.child(DatabaseConstants.CHAT_ATTENDIG_USERS).getValue();
                    if (attendingUsersList != null){
                        attendingUsersList.remove(currentUserUid);
                    }
                    chatRef.child(DatabaseConstants.CHAT_ATTENDIG_USERS).setValue(attendingUsersList);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            Intent intent = new Intent(getActivity(), MainActivity.class);
            intent.putExtra(ActivityConstants.PREVIOUS_ACTIVITY_KEY, ActivityConstants.SINGLE_TRIP_FRAGMENT);
            startActivity(intent);
        };
    }

    private void openGroupMembers(){
        Bundle bundle = new Bundle();
        bundle.putParcelable("trip", currentTrip);
        GroupMembersFragment groupMembersFragment = new GroupMembersFragment();
        groupMembersFragment.setArguments(bundle);
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, groupMembersFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getActivity().getApplicationContext());
        mapCurrent = googleMap;
        markerUtils.setLocationsMarkerOnMap((ArrayList<LocationModel>) currentTrip.getLocations(),
                mapCurrent, getActivity().getApplicationContext());
    }
}
