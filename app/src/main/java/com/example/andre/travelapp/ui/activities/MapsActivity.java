package com.example.andre.travelapp.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.andre.travelapp.R;
import com.example.andre.travelapp.constants.ActivityConstants;
import com.example.andre.travelapp.constants.DatabaseConstants;
import com.example.andre.travelapp.models.LatLong;
import com.example.andre.travelapp.models.LocationModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String TAG = "MapsActivity";

    private GoogleMap map;
    private SupportMapFragment mapFragment;
    private EditText searchLocationEditText;
    private Button completeRouteBtn;

    private ArrayList<LocationModel> places;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        onInit();

        searchLocationEditText.setOnClickListener(v -> searchLocation());

        completeRouteBtn.setOnClickListener(v -> completeRoute());

        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK) {

            /**
             *  This should be done only if you want to display a marker for the last location searched.
             *//*
                if (marker != null){
                    marker.remove();
                    Log.d(TAG, "Marker last location: " + marker.getTitle());
                }*/

            Place place = Autocomplete.getPlaceFromIntent(data);

            LocationModel newLocation = new LocationModel();
            newLocation.setAddress(place.getAddress());
            newLocation.setId(place.getId());
            newLocation.setLatLng(new LatLong(place.getLatLng().latitude, place.getLatLng().longitude));
            newLocation.setName(place.getName());
            places.add(newLocation);
            searchLocationEditText.setText(place.getAddress());

            LatLng latLng = place.getLatLng();

            map.addMarker(new MarkerOptions().position(latLng).title(place.getAddress()));
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));

            Log.d(TAG, "onActivityResult: SUCCESS");

        } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
            Log.d(TAG, "onActivityResult: ERROR ");
            Toast.makeText(this, "Something went wrong. Location could not be found.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
    }

    private void onInit() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.google_map);
        searchLocationEditText = findViewById(R.id.search_location_suggestion);
        completeRouteBtn = findViewById(R.id.complete_route_btn);
        Places.initialize(getApplicationContext(), DatabaseConstants.GOOGLE_API_KEY);
        places = new ArrayList<>();
    }

    private void searchLocation() {
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.ADDRESS,
                Place.Field.LAT_LNG, Place.Field.NAME);

        Log.d(TAG, "onClick: " + fields.get(0).toString());

        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,
                fields).build(MapsActivity.this);
        startActivityForResult(intent, 100);
    }

    private void completeRoute() {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(ActivityConstants.SELECTED_PLACES, places);
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("mapsBundle", bundle);
        intent.putExtra(ActivityConstants.PREVIOUS_ACTIVITY_KEY, ActivityConstants.MAPS_ACTIVITY);
        startActivity(intent);

    }

}
