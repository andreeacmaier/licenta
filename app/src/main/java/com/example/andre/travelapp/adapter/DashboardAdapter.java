package com.example.andre.travelapp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.andre.travelapp.R;
import com.example.andre.travelapp.constants.DatabaseConstants;
import com.example.andre.travelapp.models.LocationModel;
import com.example.andre.travelapp.models.Trip;
import com.example.andre.travelapp.models.User;
import com.example.andre.travelapp.utils.MarkerUtils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static com.firebase.ui.auth.ui.email.CheckEmailFragment.TAG;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.ViewHolder> {

    private MarkerUtils markerUtils = new MarkerUtils();

    private Context context;
    private List<Trip> trips;
    private String currentUserId;
    private DatabaseReference mRef;

    public DashboardAdapter() {
    }

    public DashboardAdapter(Context context, List<Trip> trips, String currentUserId, DatabaseReference mRef) {
        this.context = context;
        this.trips = trips;
        this.currentUserId = currentUserId;
        this.mRef = mRef;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.discover_trip_item, parent, false);
        return new DashboardAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Trip trip = trips.get(position);
        Log.d(TAG, "trip is: " + trip.getTripId());
        if (trip != null && trip.getTripId() != null) {
            Log.d(TAG, "onBindViewHolder: locations size " + trip.getLocations().size());
            List<String> attendingUsersList = trip.getAttendingUsersUids();

            holder.titleTextView.setText(trip.getTitle());
            holder.startDateTextView.setText("Departure: " + trip.getStartDate());
            holder.endDateTextView.setText("Arrival: " + trip.getEndDate());

            String limitedDescription;
            if (trip.getTripDescription() != null && trip.getTripDescription().length() > 140) {
                limitedDescription = trip.getTripDescription().substring(0, 110);
                holder.description.setText(limitedDescription + "...");
            } else {
                holder.description.setText(trip.getTripDescription());
            }

            holder.locations = (ArrayList<LocationModel>) trip.getLocations();

            if (trip.getTripType() == 0){
                holder.guideBadge.setVisibility(View.INVISIBLE);
            } else {
                holder.guideBadge.setVisibility(View.VISIBLE);
            }

            //checkIfTripAdminIsGuide(trip.getUserAdminUid(), holder.guideBadge);

            if (currentUserId.equals(trip.getUserAdminUid())) {
                holder.joinBtn.setVisibility(View.INVISIBLE);
            } else if (attendingUsersList.contains(currentUserId)) {
                holder.joinBtn.setEnabled(false);
            } else {
                holder.joinBtn.setOnClickListener(v -> updateDatabase(holder, trip, attendingUsersList));
            }
        }
    }

    private void checkIfTripAdminIsGuide(String adminUserId, ImageView guideBadge) {
        mRef.child(DatabaseConstants.USERS_DATABASE).child(adminUserId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                assert user != null;
                if (user.getUserType() == 0){
                    guideBadge.setVisibility(View.INVISIBLE);
                } else {
                    guideBadge.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void updateDatabase(@NonNull ViewHolder holder, Trip trip, List<String> finalAttendingUsersList) {
        String tripId = trip.getTripId();
        holder.joinBtn.setEnabled(false);
        finalAttendingUsersList.add(currentUserId);
        mRef.child(DatabaseConstants.TRIPS_DATABASE)
                .child(tripId)
                .child(DatabaseConstants.TRIP_USERS_LIST).setValue(finalAttendingUsersList);

        updateTripDatabase(tripId);
        updateChatDatabase(tripId);
    }

    private void updateChatDatabase(String tripId) {
        DatabaseReference chatRef = mRef.child(DatabaseConstants.CHATS_DATABASE).child(tripId);
        chatRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<String> attendingUsersIdList = (List<String>) dataSnapshot
                        .child(DatabaseConstants.CHAT_ATTENDIG_USERS).getValue();
                if (attendingUsersIdList == null) {
                    attendingUsersIdList = new ArrayList<>();
                }
                attendingUsersIdList.add(currentUserId);
                chatRef.child(DatabaseConstants.CHAT_ATTENDIG_USERS).setValue(attendingUsersIdList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void updateTripDatabase(String tripId) {
        DatabaseReference currentUserRef = mRef.child(DatabaseConstants.USERS_DATABASE).child(currentUserId);
        currentUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<String> tripsList;
                if (!dataSnapshot.hasChild(DatabaseConstants.USERS_MY_TRIPS)) {
                    currentUserRef.child(DatabaseConstants.USERS_MY_TRIPS).push();
                    tripsList = new ArrayList<>();

                } else {
                    tripsList = (List<String>) dataSnapshot.child(DatabaseConstants.USERS_MY_TRIPS).getValue();
                }
                tripsList.add(tripId);
                currentUserRef.child(DatabaseConstants.USERS_MY_TRIPS).setValue(tripsList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onViewRecycled(DashboardAdapter.ViewHolder viewHolder) {
        if (viewHolder.mapCurrent != null) {
            viewHolder.mapCurrent.clear();
            viewHolder.mapCurrent.setMapType(GoogleMap.MAP_TYPE_NONE);
        }
    }

    @Override
    public int getItemCount() {
        if (trips != null) {
            return trips.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback {

        TextView titleTextView;
        TextView startDateTextView;
        TextView endDateTextView;
        TextView description;
        Button joinBtn;
        ImageView guideBadge;

        ArrayList<LocationModel> locations;

        GoogleMap mapCurrent;
        MapView map;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            titleTextView = itemView.findViewById(R.id.list_title);
            startDateTextView = itemView.findViewById(R.id.list_startDate);
            endDateTextView = itemView.findViewById(R.id.list_endDate);
            description = itemView.findViewById(R.id.list_numberOfPeople);
            joinBtn = itemView.findViewById(R.id.joinBtn);
            guideBadge = itemView.findViewById(R.id.isGuide);

            map = itemView.findViewById(R.id.list_map);
            if (map != null) {
                map.onCreate(null);
                map.onResume();
                map.getMapAsync(this);
            }
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            MapsInitializer.initialize(context);
            mapCurrent = googleMap;
            if (trips.size() > 0 && locations != null) {
                mapCurrent.setOnMapLoadedCallback(() -> markerUtils.setLocationsMarkerOnMap(locations,
                        mapCurrent, context));
            }
        }
    }

}
