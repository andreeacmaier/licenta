package com.example.andre.travelapp.ui.activities;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.andre.travelapp.R;
import com.example.andre.travelapp.constants.ActivityConstants;
import com.example.andre.travelapp.constants.DatabaseConstants;
import com.example.andre.travelapp.models.User;
import com.example.andre.travelapp.utils.ImageUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = "TG";
    private FirebaseAuth mAuth;
    private Button registerBtn;

    private EditText usernameET, emailET, passwordET;
    private ImageView profileImage;
    private TextView signInTextView;
    private RadioGroup typeRadioGroup;
    private RadioButton radioButton;

    private Uri imageUri;
    private String userId;

    private FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference mRef = mDatabase.getReference();
    private DatabaseReference userRef;
    private ImageUtils imageUtils;
    private int userType = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();
        registerBtn = findViewById(R.id.signUp_button);
        onInit();

        registerBtn.setOnClickListener(v -> registerNewUser());
        profileImage.setOnClickListener(v -> openGallery());
        signInTextView.setOnClickListener(v -> openLoginActivity());
    }

    private void onInit() {
        usernameET = findViewById(R.id.username);
        emailET = findViewById(R.id.emailField);
        passwordET = findViewById(R.id.passwordField);
        profileImage = findViewById(R.id.profile_image);
        signInTextView = findViewById(R.id.signIn_text);
        typeRadioGroup = findViewById(R.id.typeRadioGroup);

        imageUtils = new ImageUtils(getApplicationContext());
    }

    private void registerNewUser() {
        final String username;
        final String email, password;

        username = usernameET.getText().toString();
        email = emailET.getText().toString();
        password = passwordET.getText().toString();
        int radioId = typeRadioGroup.getCheckedRadioButtonId();

        if (radioId == R.id.radio_traveller) {
            userType = 0;
        }
        if (radioId == R.id.radio_guide) {
            userType = 1;
        }

        if (username.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter name", Toast.LENGTH_LONG).show();
            return;
        }

        if (email.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter email", Toast.LENGTH_LONG).show();
            return;
        }

        if (password.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter password", Toast.LENGTH_LONG).show();
            return;
        }

        if (userType == -1) {
            Toast.makeText(getApplicationContext(), "Please select user type", Toast.LENGTH_LONG).show();
            return;
        }

        if (imageUri == null) {
            Toast.makeText(getApplicationContext(), "Please add a photo", Toast.LENGTH_LONG).show();
            return;
        }

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser firebaseUser = mAuth.getCurrentUser();

                        //verification link
                        firebaseUser.sendEmailVerification().addOnSuccessListener(aVoid ->
                                Toast.makeText(getApplicationContext(), "Verification email has been sent.",
                                Toast.LENGTH_LONG).show());

                        if (firebaseUser != null) {
                            User user = new User(email, username, userType);
                            userId = firebaseUser.getUid();
                            userRef = mRef.child(DatabaseConstants.USERS_DATABASE).child(userId);
                            userRef.setValue(user);
                            imageUtils.uploadImage(imageUri, userRef, userId);
                        }
                    } else {
                        if (password.length() < 6) {
                            Toast.makeText(getApplicationContext(), "Password should have at least 6 characters.",
                                    Toast.LENGTH_LONG).show();
                        }
                        Toast.makeText(getApplicationContext(), "Registration failed.", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, 5);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 5 && resultCode == RESULT_OK && data != null) {
            imageUri = data.getData();
            Log.d(TAG, "onActivityResult: " + imageUri);
            profileImage.setImageURI(imageUri);
        }
    }

    @Override
    public void onBackPressed() {
        openLoginActivity();
    }

    private void openLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(ActivityConstants.PREVIOUS_ACTIVITY_KEY, ActivityConstants.REGISTER_ACTIVITY);
        startActivity(intent);
    }
}
