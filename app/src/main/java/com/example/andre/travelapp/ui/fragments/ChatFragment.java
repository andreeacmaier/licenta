package com.example.andre.travelapp.ui.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.andre.travelapp.R;
import com.example.andre.travelapp.adapter.ChatAdapter;
import com.example.andre.travelapp.constants.DatabaseConstants;
import com.example.andre.travelapp.models.Chat;
import com.example.andre.travelapp.models.Message;
import com.example.andre.travelapp.models.Trip;
import com.example.andre.travelapp.models.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static androidx.constraintlayout.motion.widget.MotionScene.TAG;

public class ChatFragment extends Fragment {

    private DatabaseReference mRef;
    private DatabaseReference tripChatRef;
    private String currentUsername;
    private String currentUserUid;

    private RecyclerView recyclerView;
    private ChatAdapter chatAdapter;
    private List<Message> messages;

    private View view;
    private ImageButton sendMessageBtn;
    private EditText messageInput;

    private String chatId;
    private String title;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_chat, container, false);

        onInit();
        sendMessageBtn.setOnClickListener(v -> {
            saveMessageToDatabase();
            messageInput.setText("");
        });


        return view;
    }

    private void onInit() {
        mRef = FirebaseDatabase.getInstance().getReference();

        recyclerView = view.findViewById(R.id.chat_recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);

        chatAdapter = new ChatAdapter();
        recyclerView.setAdapter(chatAdapter);

        sendMessageBtn = view.findViewById(R.id.send_message_btn);
        messageInput = view.findViewById(R.id.message_input);

        if (getArguments() != null) {
            Chat chat = getArguments().getParcelable("chat");
            if (chat != null) {
                chatId = chat.getChatId();
                title = chat.getTitle();
            }
            Trip trip = getArguments().getParcelable("trip");
            if (trip != null) {
                chatId = trip.getTripId();
                title = trip.getTitle();
            }
            tripChatRef = mRef.child(DatabaseConstants.CHATS_DATABASE).child(chatId);
        }
        getCurrentUsername();
        displayMessages();
    }

    private void saveMessageToDatabase() {
        String message = messageInput.getText().toString();
        String newMessageKey = tripChatRef.push().getKey();

        if (TextUtils.isEmpty(message)) {
            Toast.makeText(getContext(), "Please enter a message.", Toast.LENGTH_SHORT).show();
        } else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            String currentDateTime = LocalDateTime.now().format(formatter);

            HashMap<String, Object> tripChatKey = new HashMap<>();
            tripChatRef.updateChildren(tripChatKey);

            assert newMessageKey != null;
            DatabaseReference chatMessageKeyRef = tripChatRef.child(DatabaseConstants.CHAT_MESSAGES).child(newMessageKey);

            HashMap<String, Object> messageInfoMap = new HashMap<>();
            messageInfoMap.put("uid", currentUserUid);
            messageInfoMap.put("name", currentUsername);
            messageInfoMap.put("message", message);
            messageInfoMap.put("date", currentDateTime);
            chatMessageKeyRef.updateChildren(messageInfoMap);
            
        }
    }

    private void getCurrentUsername() {
        currentUserUid = FirebaseAuth.getInstance().getUid();
        mRef.child(DatabaseConstants.USERS_DATABASE).child(currentUserUid)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        currentUsername = user.getUsername();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }


    private void displayMessages() {
        messages = new ArrayList<>();
        mRef.child(DatabaseConstants.CHATS_DATABASE).child(chatId).child(DatabaseConstants.CHAT_MESSAGES)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        messages.clear();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Message message = snapshot.getValue(Message.class);
                            messages.add(message);
                            Log.d(TAG, "onDataChange: " + snapshot.getKey());
                        }

                        chatAdapter = new ChatAdapter(getContext(), messages, getFragmentManager());
                        recyclerView.setAdapter(chatAdapter);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }
}
