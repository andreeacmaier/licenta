package com.example.andre.travelapp.ui.fragments;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.andre.travelapp.R;
import com.example.andre.travelapp.adapter.ProfileReviewAdapter;
import com.example.andre.travelapp.constants.DatabaseConstants;
import com.example.andre.travelapp.models.Review;
import com.example.andre.travelapp.models.User;
import com.example.andre.travelapp.ui.activities.LoginActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static androidx.constraintlayout.motion.widget.MotionScene.TAG;

public class ProfileFragment extends Fragment {

    private ImageView profilePicture;
    private TextView nameTextView;
    private TextView descriptionTextView;
    private Button updateBtn, logoutBtn;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private Button addReview;
    private RelativeLayout relativeLayout;

    View view;

    private FirebaseUser mUser;
    private DatabaseReference mRef;

    private String userIdToDisplay = null;

    private ProfileReviewAdapter reviewAdapter;


    @Nullable
    @Override

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        if (getArguments() != null) {
            userIdToDisplay = getArguments().getString("userId");
        }

        onInit();
        updateBtn.setOnClickListener(v -> openUpdateProfileFragment());
        logoutBtn.setOnClickListener(v -> logout());
        addReview.setOnClickListener(v -> addNewReview());

        return view;
    }


    private void onInit() {
        profilePicture = view.findViewById(R.id.profile_image);
        nameTextView = view.findViewById(R.id.name);
        descriptionTextView = view.findViewById(R.id.description);
        updateBtn = view.findViewById(R.id.update_btn);
        logoutBtn = view.findViewById(R.id.logout_Btn);
        progressBar = view.findViewById(R.id.progress);
        addReview = view.findViewById(R.id.addReview);
        relativeLayout = view.findViewById(R.id.relativeLayout);

        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        reviewAdapter = new ProfileReviewAdapter();
        recyclerView.setAdapter(reviewAdapter);

        mRef = FirebaseDatabase.getInstance().getReference();
        mUser = FirebaseAuth.getInstance().getCurrentUser();

        displayUserDetails();
    }

    private void displayUserDetails() {
        DatabaseReference userRef, reviewRef;
        if (userIdToDisplay != null) {
            userRef = mRef.child(DatabaseConstants.USERS_DATABASE).child(userIdToDisplay);
            reviewRef = mRef.child(DatabaseConstants.REVIEWS_DATABASE).child(userIdToDisplay);
            updateBtn.setVisibility(View.INVISIBLE);
            logoutBtn.setVisibility(View.INVISIBLE);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)
                    relativeLayout.getLayoutParams();
            params.addRule(RelativeLayout.BELOW, R.id.description);
            relativeLayout.setLayoutParams(params);
        } else {
            userRef = mRef.child(DatabaseConstants.USERS_DATABASE).child(mUser.getUid());
            reviewRef = mRef.child(DatabaseConstants.REVIEWS_DATABASE).child(mUser.getUid());
            addReview.setVisibility(View.INVISIBLE);
        }

        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                nameTextView.setText(user.getUsername());
                if (dataSnapshot.hasChild(DatabaseConstants.USERS_DESCRIPTION)) {
                    descriptionTextView.setText(user.getDescription());
                }
                setProfilePicture(user.getProfilePicture());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        List<Review> reviews = new ArrayList<>();

        reviewRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                reviews.clear();
                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    System.out.println(snapshot);
                    Review review = snapshot.getValue(Review.class);
                    Log.d(TAG, "display reviews: " + review.getReview());
                    reviews.add(review);
                }

                reviewAdapter = new ProfileReviewAdapter(getContext(), reviews, getFragmentManager());
                recyclerView.setAdapter(reviewAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void setProfilePicture(String imageUri) {
        if (isAdded()) {
            Glide.with(getContext()).
                    load(imageUri)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(profilePicture);
        }
    }

    private void addNewReview(){
        Bundle bundle = new Bundle();
        bundle.putString("userId", userIdToDisplay);
        Log.d(TAG, "addNewReview: " + userIdToDisplay);
        NewReviewFragment newReviewFragment = new NewReviewFragment();
        newReviewFragment.setArguments(bundle);
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, newReviewFragment)
                .addToBackStack(null).commit();
    }

    private void logout() {
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void openUpdateProfileFragment() {
        UpdateProfileFragment updateProfileFragment = new UpdateProfileFragment();
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, updateProfileFragment)
                .addToBackStack(null).commit();
    }
}
