package com.example.andre.travelapp.ui.activities;

import android.content.Intent;
import android.os.Bundle;

import com.example.andre.travelapp.R;
import com.example.andre.travelapp.constants.ActivityConstants;
import com.example.andre.travelapp.ui.fragments.DashboardFragment;
import com.example.andre.travelapp.ui.fragments.InboxFragment;
import com.example.andre.travelapp.ui.fragments.MyTripsFragment;
import com.example.andre.travelapp.ui.fragments.NewTripFragment;
import com.example.andre.travelapp.ui.fragments.ProfileFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = "Log TAG ";

    String previousActivity;
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        previousActivity = getIntent().getStringExtra(ActivityConstants.PREVIOUS_ACTIVITY_KEY);

        bottomNavigationView = findViewById(R.id.bottom_navigation_bar);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

        if (previousActivity != null && previousActivity.equals(ActivityConstants.MAPS_ACTIVITY)) {
            NewTripFragment newTripFragment = new NewTripFragment();
            Bundle bundle = getIntent().getBundleExtra("mapsBundle");
            bundle.putString(ActivityConstants.PREVIOUS_FRAGMENT_KEY, ActivityConstants.DASHBOARD_ACTIVITY);
            newTripFragment.setArguments(bundle);
            bottomNavigationView.setSelectedItemId(R.id.nav_new_trip);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    newTripFragment).addToBackStack("this").commit();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new DashboardFragment()).commit();
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            item -> {
                Fragment selectedFragment = null;
                int selectedItemId = bottomNavigationView.getSelectedItemId();

                if (selectedItemId != item.getItemId()) {
                    switch (item.getItemId()) {
                        case R.id.nav_home:
                            selectedFragment = new DashboardFragment();
                            break;
                        case R.id.nav_new_trip:
                            selectedFragment = new NewTripFragment();
                            break;
                        case R.id.nav_my_trips:
                            selectedFragment = new MyTripsFragment();
                            break;
                        case R.id.nav_inbox:
                            selectedFragment = new InboxFragment();
                            break;
                        case R.id.nav_profile:
                            selectedFragment = new ProfileFragment();
                            break;
                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            selectedFragment).commit();

                    return true;
                } else {
                    return false;
                }
            };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
