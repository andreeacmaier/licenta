package com.example.andre.travelapp.ui.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.andre.travelapp.R;
import com.example.andre.travelapp.constants.ActivityConstants;
import com.example.andre.travelapp.constants.DatabaseConstants;
import com.example.andre.travelapp.models.LocationModel;
import com.example.andre.travelapp.models.Trip;
import com.example.andre.travelapp.models.User;
import com.example.andre.travelapp.ui.activities.MainActivity;
import com.example.andre.travelapp.ui.activities.MapsActivity;
import com.example.andre.travelapp.utils.MarkerUtils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.libraries.places.api.Places;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

@RequiresApi(api = Build.VERSION_CODES.O)
public class NewTripFragment extends Fragment implements OnMapReadyCallback {

    private final static String TAG = "Log TAG ";
    private static final int SAVE_ACTION = 0;
    private static final int UPDATE_ACTION = 1;

    private DatabaseReference mRef;
    private String currentUserId;
    private View view;

    private EditText titleView, tripDescription;
    private Button selectLocationsBtn, startDateBtn, endDateBtn, createTripBtn;
    private TextView errorTitleView, errorLocationsView, errorStartDateView,
            errorEndDateView, errorNumberOfPeopleView;
    private RadioGroup typeRadioGroup;

    private StringBuilder date;
    private List<LocationModel> selectedPlaces;
    private MarkerUtils markerUtils;
    private String updatedTripId;
    private boolean isPreviousActivityMaps = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        int action = selectAction();
        getSelectedLocationsFromMapsActivity();

        view = inflater.inflate(R.layout.fragment_new_trip, container, false);
        onInit();
        if (action == UPDATE_ACTION) {
            setFieldsForUpdate();
        }

        titleView.setOnClickListener(v -> hideErrorMessages());
        selectLocationsBtn.setOnClickListener(v -> {
            hideErrorMessages();
            searchLocations();
        });

        startDateBtn.setOnClickListener(v -> {
            hideErrorMessages();
            setStartDate(v);
        });
        endDateBtn.setOnClickListener(v -> {
            hideErrorMessages();
            setEndDate(v);
        });
        tripDescription.setOnClickListener(v -> hideErrorMessages());
        createTripBtn.setOnClickListener(v -> createTrip());

        return view;
    }

    private void onInit() {
        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        mRef = mDatabase.getReference();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        currentUserId = mAuth.getCurrentUser().getUid();

        titleView = view.findViewById(R.id.titleField);
        tripDescription = view.findViewById(R.id.tripDescription);
        selectLocationsBtn = view.findViewById(R.id.selectLocations_btn);
        startDateBtn = view.findViewById(R.id.startDate_btn);
        endDateBtn = view.findViewById(R.id.endDate_btn);
        createTripBtn = view.findViewById(R.id.createBtn);
        MapView routeView = view.findViewById(R.id.mapView);
        errorTitleView = view.findViewById(R.id.errorTitle);
        errorLocationsView = view.findViewById(R.id.errorSelectLocations);
        errorEndDateView = view.findViewById(R.id.errorEndDate);
        errorStartDateView = view.findViewById(R.id.errorStartDate);
        errorNumberOfPeopleView = view.findViewById(R.id.errorNumberOfPeople);
        typeRadioGroup = view.findViewById(R.id.typeRadioGroup);

        mRef.child(DatabaseConstants.USERS_DATABASE).child(currentUserId)
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                assert user != null;
                if (user.getUserType() == 0 ){
                    typeRadioGroup.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        markerUtils = new MarkerUtils();
        Places.initialize(requireActivity(), DatabaseConstants.GOOGLE_API_KEY);

        if (routeView != null) {
            routeView.onCreate(null);
            routeView.onResume();
            routeView.getMapAsync(this);
        }
    }

    /**
     * Checks which type of action will be performed
     */
    private int selectAction() {
        if (getArguments() != null) {
            if (getArguments().getString(ActivityConstants.PREVIOUS_FRAGMENT_KEY).equals(ActivityConstants.DASHBOARD_ACTIVITY)) {
                selectedPlaces = getArguments().getParcelableArrayList("selectedPlaces");
                return SAVE_ACTION;
            } else if (getArguments().getString(ActivityConstants.PREVIOUS_FRAGMENT_KEY).equals(ActivityConstants.MY_TRIPS_FRAGMENT)) {
                return UPDATE_ACTION;
            }
        }
        return -1;
    }

    /**
     * Opens MapsActivity so the user can choose the locations from map
     */
    private void searchLocations() {
        Log.d(TAG, "searchLocations() ");
        Intent intent = new Intent(requireContext(), MapsActivity.class);
        if (selectedPlaces != null && selectedPlaces.size() > 0){
            intent.putParcelableArrayListExtra("selectedPlaces", (ArrayList<? extends Parcelable>) selectedPlaces);
        }
        startActivity(intent);
    }

    /**
     * Receives extras from MapsActivity with selected locations
     * and displays their addresses in a list
     */
    private void getSelectedLocationsFromMapsActivity() {
        if (getArguments() != null) {
            if (getArguments().getString(ActivityConstants.PREVIOUS_FRAGMENT_KEY).equals(ActivityConstants.DASHBOARD_ACTIVITY)) {
                selectedPlaces = getArguments().getParcelableArrayList(ActivityConstants.SELECTED_PLACES);
                getArguments().clear();
                isPreviousActivityMaps = true;
            }
        }
    }

    /**
     * Takes the data from every field and creates a Trip object which will be saved
     * to db for this user.
     */
    private void createTrip() {
        Log.d(TAG, "NewTripFragment was selected from menu bar.");
        String title = titleView.getText().toString();
        String description = tripDescription.getText().toString().trim();
        int tripType = 0;
        int radioId = typeRadioGroup.getCheckedRadioButtonId();

        if (radioId == R.id.radio_guide) {
            tripType = 1;
        }

        if (isFormValid(title, description, startDateBtn.getText().toString(),
                endDateBtn.getText().toString())) {
            Trip trip = new Trip(title, startDateBtn.getText().toString(),
                    endDateBtn.getText().toString(),
                    selectedPlaces, description, currentUserId, tripType);
            trip.setInsertedTimestamp(getCurrentDate());
            if (!createTripBtn.getText().equals(ActivityConstants.UPDATE_BTN)) {
                saveTripToDatabase(trip);
                updateUserTrips(trip);
                Log.d(TAG, "Trip with title \"" + trip.getTitle() + "\" was saved to database.");
            } else {
                updateTripInDatabase(trip);
            }

            Intent intent = new Intent(getActivity(), MainActivity.class);
            startActivity(intent);
        }
    }

    private String formatDate(String dateFromPicker){
        String[] text = dateFromPicker.split(" ");
        String[] date = text[0].split("-");
        String[] time = text[1].split(":");

        String year = date[0];
        String month = date[1];
        String day = date[2];

        String hour = time[0];
        String minute = time[1];

        String zero = "0";
        String dbDate;

        if (month.length() == 1 ){
            month = zero.concat(month);
        }
        if (day.length() == 1 ){
            day = zero.concat(day);
        }
        if (hour.length() == 1 ){
            hour = zero.concat(hour);
        }
        if (minute.length() == 1){
            minute = zero.concat(minute);
        }

        dbDate = year + "-" + month + "-" + day +
                " " + hour + ":" + minute;

        return dbDate;
    }

    private void saveTripToDatabase(Trip trip) {
        Log.d(TAG, "saveTripToDatabase()");
        DatabaseReference tripsRef = mRef.child(DatabaseConstants.TRIPS_DATABASE);
        DatabaseReference newTripRef = tripsRef.push();
        String tripId = newTripRef.getKey();
        trip.setTripId(tripId);
        newTripRef.setValue(trip);
        saveChatToDatabase(tripId, trip.getTitle());
    }

    private void saveChatToDatabase(String tripId, String title) {
        Log.d(TAG, "saveChatToDatabase()");
        DatabaseReference chatRef = mRef.child(DatabaseConstants.CHATS_DATABASE).child(tripId);
        chatRef.child(DatabaseConstants.CHAT_TITLE).setValue(title);
        List<String> attendingUsersList = new ArrayList<>();
        attendingUsersList.add(currentUserId);
        chatRef.child(DatabaseConstants.CHAT_ATTENDIG_USERS).setValue(attendingUsersList);
    }

    private void updateUserTrips(Trip trip) {
        Log.d(TAG, "updateUserTrips()");
        DatabaseReference currentUserRef = mRef.child(DatabaseConstants.USERS_DATABASE)
                .child(currentUserId);
        currentUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<String> tripsList;
                if (!dataSnapshot.hasChild(DatabaseConstants.USERS_MY_TRIPS)) {
                    currentUserRef.child(DatabaseConstants.USERS_MY_TRIPS).push();
                    tripsList = new ArrayList<>();

                } else {
                    tripsList = (List<String>) dataSnapshot.child(DatabaseConstants.USERS_MY_TRIPS).getValue();
                }
                tripsList.add(trip.getTripId());
                currentUserRef.child(DatabaseConstants.USERS_MY_TRIPS).setValue(tripsList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void updateTripInDatabase(Trip trip) {
        DatabaseReference tripRef = mRef.child(DatabaseConstants.TRIPS_DATABASE)
                .child(updatedTripId);
        trip.setTripId(updatedTripId);
        tripRef.setValue(trip);
    }

    private boolean isFormValid(String title, String description, String startDate, String endDate) {
        Log.d(TAG, "checkIsFormValid");
        boolean isValid = true;

        if (TextUtils.isEmpty(title)) {
            errorTitleView.setVisibility(View.VISIBLE);
            isValid = false;
        }
        if (TextUtils.isEmpty(description)) {
            errorNumberOfPeopleView.setVisibility(View.VISIBLE);
            isValid = false;
        }
        if (TextUtils.isEmpty(startDateBtn.getText().toString())) {
            errorStartDateView.setVisibility(View.VISIBLE);
            isValid = false;
        }
        if (TextUtils.isEmpty(endDateBtn.getText().toString())) {
            errorEndDateView.setVisibility(View.VISIBLE);
            isValid = false;
        }
        if (startDate.compareTo(endDate) >= 0){
            errorEndDateView.setText("Arrival date must be after departure date.");
            errorEndDateView.setVisibility(View.VISIBLE);
            isValid = false;
        }
        if ((isPreviousActivityMaps && (selectedPlaces == null || selectedPlaces.size() < 1)) ||
                selectedPlaces == null){
            errorLocationsView.setVisibility(View.VISIBLE);
            isValid = false;
        }

        return isValid;
    }

    /**
     * Displays date/time picker and appends selected date and time to the date object
     */
    private void displayDateTimePicker(final View v) {
        date = new StringBuilder();
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), (view, year, month, dayOfMonth) -> {
            date.append(year + "-" + (month + 1) + "-" + dayOfMonth);
            displayTimePicker(v);
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePicker.show();
    }

    private void displayTimePicker(final View v) {
        Calendar calendar = Calendar.getInstance();
        TimePickerDialog timePicker = new TimePickerDialog(getActivity(), (view, hourOfDay, minute) -> {
            date.append(" " + hourOfDay + ":" + minute);
            if (v.getId() == R.id.startDate_btn) {
                Button btn = (Button) v;
                btn.setText(formatDate(date.toString()));
            } else if (v.getId() == R.id.endDate_btn) {
                Button btn = (Button) v;
                btn.setText(formatDate(date.toString()));
            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),
                android.text.format.DateFormat.is24HourFormat(getActivity()));
        timePicker.show();
    }


    private void setStartDate(View v) {
        displayDateTimePicker(v);
    }

    private void setEndDate(View v) {
        displayDateTimePicker(v);
    }

    private void hideErrorMessages() {
        errorTitleView.setVisibility(View.INVISIBLE);
        errorLocationsView.setVisibility(View.INVISIBLE);
        errorStartDateView.setVisibility(View.INVISIBLE);
        errorEndDateView.setVisibility(View.INVISIBLE);
        errorNumberOfPeopleView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getActivity());
        if (selectedPlaces != null) {
            if (selectedPlaces.size() > 0) {
                markerUtils.setLocationsMarkerOnMap((ArrayList<LocationModel>) selectedPlaces, googleMap, getActivity());
            }
        }
    }

    private void setFieldsForUpdate() {
        Trip trip = getArguments().getParcelable("trip");
        updatedTripId = trip.getTripId();

        titleView.setText(trip.getTitle());
        startDateBtn.setText(trip.getStartDate());
        endDateBtn.setText(trip.getEndDate());
        tripDescription.setText(trip.getTripDescription() + "");
        selectedPlaces = trip.getLocations();

        createTripBtn.setText(ActivityConstants.UPDATE_BTN);
    }


    private String getCurrentDate(){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime currentDate = LocalDateTime.now();
        return dateTimeFormatter.format(currentDate);
    }
}
