package com.example.andre.travelapp.scheduler;

import android.util.Log;

import com.example.andre.travelapp.constants.DatabaseConstants;
import com.example.andre.travelapp.models.Trip;
import com.example.andre.travelapp.models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;

import static androidx.constraintlayout.motion.widget.MotionScene.TAG;

public class Scheduler {

    private static DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    public void deleteFinishedTrips() {
        mRef.child(DatabaseConstants.TRIPS_DATABASE)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Trip trip = snapshot.getValue(Trip.class);
                            assert trip != null;

                            LocalDateTime now = LocalDateTime.now();
                            String tripId = trip.getTripId();
                            LocalDateTime ldtEndDate = LocalDateTime.parse(trip.getEndDate(), formatter);

                            if (now.compareTo(ldtEndDate) > 0) {
                                // delete from trips and user's trip list
                                Log.d(TAG, "SCHEDULER : " + now + " enddate : " + ldtEndDate + "   compare = " + (now.compareTo(ldtEndDate) > 0));
                                remove(tripId);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void remove(String tripId) {
        FirebaseDatabase.getInstance().getReference().child(DatabaseConstants.TRIPS_DATABASE)
                .child(tripId).removeValue();

        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child(DatabaseConstants.USERS_DATABASE);
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String key = snapshot.getKey();
                    User user = snapshot.getValue(User.class);
                    List<String> userTripsList = (ArrayList<String>) snapshot
                            .child(DatabaseConstants.USERS_MY_TRIPS).getValue();
                    assert user != null;
                    Log.d(TAG, "deleteTripListener: " + user.getUsername());
                    if (userTripsList != null && userTripsList.contains(tripId)) {
                        Log.d(TAG, "deleteTripListener: ");
                        userTripsList.remove(tripId);
                        user.setMyTripsUid(userTripsList);
                        userRef.child(key).setValue(user);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
