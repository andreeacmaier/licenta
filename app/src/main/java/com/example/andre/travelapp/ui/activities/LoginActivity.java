package com.example.andre.travelapp.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.andre.travelapp.R;
import com.example.andre.travelapp.constants.ActivityConstants;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.sql.Timestamp;

import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {

    private final static String TAG = "Log TAG ";

    private TextView signUpTV, forgotPasswordTV, emailNotVerified;
    private EditText emailET, passwordET;
    private Button loginBtn;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Timestamp start = new Timestamp(System.currentTimeMillis());
        Log.d(TAG, "Application started: " + start);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Timestamp stop = new Timestamp(System.currentTimeMillis());
        long displayTime = stop.getTime()-start.getTime();
        Log.d(TAG, "Login activity displayed in : " + displayTime + " ms.");

        mAuth = FirebaseAuth.getInstance();
        onInit();

        if (mAuth.getCurrentUser() != null) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }

        loginBtn.setOnClickListener(v -> login());
        signUpTV.setOnClickListener(v -> goToRegister());
        forgotPasswordTV.setOnClickListener(v -> openForgotPassword());
    }

    private void onInit() {
        signUpTV = findViewById(R.id.signUp_text);
        emailET = findViewById(R.id.emailField);
        passwordET = findViewById(R.id.passwordField);
        loginBtn = findViewById(R.id.signIn_button);
        forgotPasswordTV = findViewById(R.id.reset_text);
        emailNotVerified = findViewById(R.id.email_not_verified);
    }

    private void goToRegister() {
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
        finish();
    }

    private void login() {
        final String email, password;

        email = emailET.getText().toString();
        password = passwordET.getText().toString();

        if (email.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Email field is empty", Toast.LENGTH_LONG).show();
            return;
        }

        if (password.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Password field is empty", Toast.LENGTH_LONG).show();
            return;
        }

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(LoginActivity.this, task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser mUser = mAuth.getCurrentUser();
                        if (mUser.isEmailVerified()) {
                            Log.d(TAG, "User successfully authenticated.");
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.putExtra(ActivityConstants.PREVIOUS_ACTIVITY_KEY, ActivityConstants.LOGIN_ACTIVITY);
                            startActivity(intent);
                        } else {
                            emailNotVerified.setVisibility(View.VISIBLE);
                            mAuth.signOut();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Username or password incorrect", Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void openForgotPassword() {
        Intent intent = new Intent(LoginActivity.this, ResetPasswordActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }
}
