package com.example.andre.travelapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Chat implements Parcelable {

    private String chatId;
    private String title;
    private Message message;

    public Chat() {
    }


    protected Chat(Parcel in) {
        chatId = in.readString();
        title = in.readString();
        message = in.readParcelable(Message.class.getClassLoader());
    }

    public static final Creator<Chat> CREATOR = new Creator<Chat>() {
        @Override
        public Chat createFromParcel(Parcel in) {
            return new Chat(in);
        }

        @Override
        public Chat[] newArray(int size) {
            return new Chat[size];
        }
    };

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(chatId);
        dest.writeString(title);
        dest.writeParcelable(message, flags);
    }


}
