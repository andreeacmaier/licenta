package com.example.andre.travelapp.constants;

public class DatabaseConstants {

    public static final String USERS_DATABASE = "users";
    public static final String TRIPS_DATABASE = "trips";
    public static final String CHATS_DATABASE = "chats";

    public static final String USERS_EMAIL = "email";
    public static final String USERS_USERNAME = "username";
    public static final String USERS_MY_TRIPS = "myTripsUid";
    public static final String USERS_TYPE = "userType";

    public static final String TRIP_TITLE = "title";
    public static final String TRIP_START_DATE = "startDate";
    public static final String TRIP_END_DATE = "endDate";
    public static final String TRIP_SELECTED_PLACES = "selectedPlaces";
    public static final String TRIP_DESCRIPTION = "tripDescription";
    public static final String TRIP_USERS_LIST = "attendingUsersUids";
    public static final String GOOGLE_API_KEY = "AIzaSyCkVy3hQqZg1lrCtQ_BHjFUJLkdjYNXPLk";

    public static final String PROFILE_PICTURES_STORAGE = "profilePictures";
    public static final String USERS_PROFILE_PICTURE = "profilePicture";
    public static final String USERS_DESCRIPTION = "description";
    public static final String CHAT_TITLE = "title";
    public static final String CHAT_ATTENDIG_USERS = "attendingUsers";
    public static final String CHAT_MESSAGES = "messages";
    public static final String REVIEWS_DATABASE = "reviews";
}
