package com.example.andre.travelapp.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.andre.travelapp.R;
import com.example.andre.travelapp.adapter.GroupMembersAdaptor;
import com.example.andre.travelapp.constants.DatabaseConstants;
import com.example.andre.travelapp.models.GroupMember;
import com.example.andre.travelapp.models.Trip;
import com.example.andre.travelapp.models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class GroupMembersFragment extends Fragment {

    private DatabaseReference mRef;

    private View view;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private GroupMembersAdaptor groupMembersAdaptor;

    private String tripId;
    private List<GroupMember> usersList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_group_members, container, false);

        onInit();
        return view;
    }

    private void onInit() {
        recyclerView = view.findViewById(R.id.linear_recyclerview);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        groupMembersAdaptor = new GroupMembersAdaptor();
        recyclerView.setAdapter(groupMembersAdaptor);

        mRef = FirebaseDatabase.getInstance().getReference();
        usersList = new ArrayList<>();

        if (getArguments() != null && !getArguments().isEmpty()) {
            Trip trip = getArguments().getParcelable("trip");
            assert trip != null;
            tripId = trip.getTripId();
            displayGroupMembers();
        }
    }

    private void displayGroupMembers() {
        mRef.child(DatabaseConstants.USERS_DATABASE).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User user = snapshot.getValue(User.class);
                    assert user != null;
                    List<String> tripsList = user.getMyTripsUid();
                    if (tripsList != null && tripsList.contains(tripId)) {
                        GroupMember member = new GroupMember(user.getUsername(), user.getProfilePicture(),
                            snapshot.getKey());
                        usersList.add(member);
                    }
                }

                if (isAdded()) {
                    groupMembersAdaptor = new GroupMembersAdaptor(getContext(), usersList, getFragmentManager());
                    recyclerView.setAdapter(groupMembersAdaptor);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


}
