package com.example.andre.travelapp.utils;

import android.content.Context;
import android.util.Log;

import com.example.andre.travelapp.R;
import com.example.andre.travelapp.models.LocationModel;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import static com.android.volley.VolleyLog.TAG;

public class MarkerUtils {

    public void setLocationsMarkerOnMap(ArrayList<LocationModel> locations, GoogleMap googleMap, Context context) {
        ArrayList<Marker> markers = new ArrayList<>();
        Log.d(TAG, "setLocationsMarkerOnMap: " + locations);
        for (LocationModel location : locations) {
            LatLng latLng = new LatLng(location.getLatLng().getLat(), location.getLatLng().getLng());
            Marker marker = googleMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(location.getName())
                    .snippet(location.getName())
                    .icon(BitmapGenerator.generateBitmapDescriptorFromRes(context,
                            R.drawable.ic_place_accent_24dp))
            );
            markers.add(marker);
        }

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(getBounds(markers), 100);
        googleMap.moveCamera(cameraUpdate);
    }

    public LatLngBounds getBounds(ArrayList<Marker> markers) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker marker : markers) {
            builder.include(marker.getPosition());
        }
        return builder.build();
    }
}
