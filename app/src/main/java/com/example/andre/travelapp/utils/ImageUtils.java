package com.example.andre.travelapp.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.example.andre.travelapp.constants.DatabaseConstants;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import static androidx.constraintlayout.motion.widget.MotionScene.TAG;

public class ImageUtils {

    private Context context;

    public ImageUtils(Context context) {
        this.context = context;
    }

    public void uploadImage(Uri imageUri, DatabaseReference userRef, String userId) {
        if (imageUri != null) {
            StorageReference storageReference = FirebaseStorage.getInstance().getReference()
                    .child(DatabaseConstants.PROFILE_PICTURES_STORAGE)
                    .child(userId + getFileExtension(imageUri));

            Log.d(TAG, "uploadImage: " + imageUri);

            UploadTask uploadTask = storageReference.putFile(imageUri);
            uploadTask.continueWithTask((Continuation<UploadTask.TaskSnapshot, Task<Uri>>) task -> {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                return storageReference.getDownloadUrl();
            }).addOnCompleteListener((OnCompleteListener<Uri>) task -> {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    String profilePictureUri = downloadUri.toString();

                    userRef.child(DatabaseConstants.USERS_PROFILE_PICTURE).setValue(profilePictureUri);

                } else {
                    Log.d(TAG, "Failed uploading image");

                }
            }).addOnFailureListener(e -> Toast.makeText(context, "Failed uploading image", Toast.LENGTH_SHORT).show());
        }
    }

    private String getFileExtension(Uri uri) {
        ContentResolver contentResolver = context.getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

}
