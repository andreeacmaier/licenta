package com.example.andre.travelapp.constants;

import java.util.Arrays;
import java.util.List;

public class ActivityConstants {

    public static final String PREVIOUS_ACTIVITY_KEY = "previousActivity";
    public static final String PREVIOUS_FRAGMENT_KEY = "previousFragment";

    public static final String LOGIN_ACTIVITY="LoginActivity";
    public static final String REGISTER_ACTIVITY="RegisterActivity";
    public static final String DISCOVER_ACTIVITY="DiscoverActivity";
    public static final String NEW_TRIP_FRAGMENT ="NewTripFragment";
    public static final String SINGLE_TRIP_FRAGMENT ="SingleTripFragment";
    public static final String DASHBOARD_ACTIVITY="MainActivity";
    public static final String MAPS_ACTIVITY="MapsActivity";
    public static final String MY_TRIPS_FRAGMENT = "MyTripsFragment";

    public static final String SELECTED_PLACES = "placesArrayList";

    public static final String UPDATE_BTN = "UPDATE";

    public static final List<String> DROPDOWN_OPTIONS =
            Arrays.asList("Date added", "Start Date asc", "Start Date desc", "Near me");
}
