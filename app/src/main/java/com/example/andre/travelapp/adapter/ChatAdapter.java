package com.example.andre.travelapp.adapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.andre.travelapp.R;
import com.example.andre.travelapp.constants.DatabaseConstants;
import com.example.andre.travelapp.models.Message;
import com.example.andre.travelapp.ui.fragments.ProfileFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import static androidx.constraintlayout.motion.widget.MotionScene.TAG;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    public static final int MSG_TYPE_THIS_USER = 0;
    public static final int MSG_TYPE_GROUP = 1;

    private Context context;
    private List<Message> messages;
    private FragmentManager fragmentManager;

    private FirebaseUser mUser;

    public ChatAdapter() {
    }

    public ChatAdapter(Context context, List<Message> messages, FragmentManager fragmentManager) {
        this.context = context;
        this.messages = messages;
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == MSG_TYPE_THIS_USER) {
            View view = LayoutInflater.from(context).inflate(R.layout.chat_item_right, parent, false);
            return new ChatAdapter.ViewHolder(view);
        } else {
            View view = LayoutInflater.from(context).inflate(R.layout.chat_item_left, parent, false);
            return new ChatAdapter.ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ChatAdapter.ViewHolder holder, int position) {
        Message message = messages.get(position);
        holder.userMessageTextView.setText(message.getMessage());

        if (this.getItemViewType(position) == MSG_TYPE_GROUP) {
            holder.userDetailsTextView.setText(message.getName());
            holder.messageDateTextView.setText(message.getDate());
        }

        FirebaseDatabase.getInstance().getReference().child(DatabaseConstants.USERS_DATABASE)
                .child(message.getUid()).child(DatabaseConstants.USERS_PROFILE_PICTURE).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String imageUri = dataSnapshot.getValue(String.class);
                if (imageUri != null) {
                    Glide.with(context).
                            load(imageUri)
                            .placeholder(R.drawable.explorer)
                            .into(holder.profileImage);
                } else {
                    Log.d(TAG, "ERROR LOADING IMAGE URI: ");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        holder.profileImage.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("userId", message.getUid());
            ProfileFragment profileFragment = new ProfileFragment();
            profileFragment.setArguments(bundle);
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, profileFragment)
                    .addToBackStack(null)
                    .commit();
        });

    }

    @Override
    public int getItemCount() {
        if (messages != null) {
            return messages.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView userDetailsTextView;
        TextView userMessageTextView;
        TextView messageDateTextView;
        ImageView profileImage;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            userDetailsTextView = itemView.findViewById(R.id.user_details);
            userMessageTextView = itemView.findViewById(R.id.user_message);
            messageDateTextView = itemView.findViewById(R.id.message_date);
            profileImage = itemView.findViewById(R.id.profile_image);
        }
    }

    @Override
    public int getItemViewType(int position) {
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        if (messages != null && messages.get(position).getUid().equals(mUser.getUid())) {
            return MSG_TYPE_THIS_USER;
        } else {
            return MSG_TYPE_GROUP;
        }
    }

}
