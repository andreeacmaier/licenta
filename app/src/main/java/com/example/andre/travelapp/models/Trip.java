package com.example.andre.travelapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Trip implements Parcelable {

    private String tripId;
    private String title;
    private String startDate;
    private String endDate;
    private List<LocationModel> locations;
    private String tripDescription;
    private String userAdminUid;
    private List<String> attendingUsersUids;
    private String insertedTimestamp;
    private int tripType;

    public Trip(){}

    public Trip(String title, String startDate, String endDate, List<LocationModel> locations, String tripDescription, String userAdminUid, int tripType) {
        this.title = title;
        this.startDate = startDate;
        this.endDate = endDate;
        this.locations = locations;
        this.tripDescription = tripDescription;
        this.userAdminUid = userAdminUid;
        this.attendingUsersUids = new ArrayList<>();
        this.attendingUsersUids.add(userAdminUid);
        this.tripType = tripType;
    }


    protected Trip(Parcel in) {
        tripId = in.readString();
        title = in.readString();
        startDate = in.readString();
        endDate = in.readString();
        locations = in.createTypedArrayList(LocationModel.CREATOR);
        tripDescription = in.readString();
        userAdminUid = in.readString();
        attendingUsersUids = in.createStringArrayList();
        insertedTimestamp = in.readString();
        tripType = in.readInt();
    }

    public static final Creator<Trip> CREATOR = new Creator<Trip>() {
        @Override
        public Trip createFromParcel(Parcel in) {
            return new Trip(in);
        }

        @Override
        public Trip[] newArray(int size) {
            return new Trip[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<LocationModel> getLocations() {
        return locations;
    }

    public void setLocations(List<LocationModel> locations) {
        this.locations = locations;
    }

    public String getTripDescription() {
        return tripDescription;
    }

    public void setTripDescription(String tripDescription) {
        this.tripDescription = tripDescription;
    }

    public String getUserAdminUid() {
        return userAdminUid;
    }

    public void setUserAdminUid(String userAdminUid) {
        this.userAdminUid = userAdminUid;
    }

    public List<String> getAttendingUsersUids() {
        return attendingUsersUids;
    }

    public void setAttendingUsersUids(List<String> attendingUsersUids) {
        this.attendingUsersUids = attendingUsersUids;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getInsertedTimestamp() {
        return insertedTimestamp;
    }

    public void setInsertedTimestamp(String insertedTimestamp) {
        this.insertedTimestamp = insertedTimestamp;
    }

    public int getTripType() {
        return tripType;
    }

    public void setTripType(int tripType) {
        this.tripType = tripType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tripId);
        dest.writeString(title);
        dest.writeString(startDate);
        dest.writeString(endDate);
        dest.writeTypedList(locations);
        dest.writeString(tripDescription);
        dest.writeString(userAdminUid);
        dest.writeStringList(attendingUsersUids);
        dest.writeString(insertedTimestamp);
        dest.writeInt(tripType);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trip trip = (Trip) o;
        return tripType == trip.tripType &&
                Objects.equals(tripId, trip.tripId) &&
                Objects.equals(title, trip.title) &&
                Objects.equals(startDate, trip.startDate) &&
                Objects.equals(endDate, trip.endDate) &&
                Objects.equals(locations, trip.locations) &&
                Objects.equals(tripDescription, trip.tripDescription) &&
                Objects.equals(userAdminUid, trip.userAdminUid) &&
                Objects.equals(attendingUsersUids, trip.attendingUsersUids) &&
                Objects.equals(insertedTimestamp, trip.insertedTimestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tripId, title, startDate, endDate, locations, tripDescription, userAdminUid, attendingUsersUids, insertedTimestamp, tripType);
    }
}
