package com.example.andre.travelapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class User implements Parcelable {

    private String email;
    private String username;
    private List<String> myTripsUid;
    private String profilePicture;
    private String description;
    private int userType;

    public User() {
    }

    public User(String email, String username, int userType) {
        this.email = email;
        this.username = username;
        this.userType = userType;
    }

    public User(String email, String username, List<String> myTripsUid) {
        this.email = email;
        this.username = username;
        this.myTripsUid = myTripsUid;
    }

    protected User(Parcel in) {
        email = in.readString();
        username = in.readString();
        myTripsUid = in.createStringArrayList();
        profilePicture = in.readString();
        description = in.readString();
        userType = in.readInt();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getMyTripsUid() {
        return myTripsUid;
    }

    public void setMyTripsUid(List<String> myTripsUid) {
        this.myTripsUid = myTripsUid;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
        dest.writeString(username);
        dest.writeStringList(myTripsUid);
        dest.writeString(profilePicture);
        dest.writeString(description);
        dest.writeInt(userType);
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", myTripsUid=" + myTripsUid +
                ", profilePicture='" + profilePicture + '\'' +
                ", description='" + description + '\'' +
                ", userType=" + userType +
                '}';
    }
}
