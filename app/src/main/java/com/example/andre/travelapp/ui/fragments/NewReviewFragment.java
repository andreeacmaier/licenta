package com.example.andre.travelapp.ui.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.andre.travelapp.R;
import com.example.andre.travelapp.constants.DatabaseConstants;
import com.example.andre.travelapp.models.Review;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import static com.firebase.ui.auth.ui.email.CheckEmailFragment.TAG;

public class NewReviewFragment extends Fragment {

    private View view;
    private TextView error;
    private EditText review;
    private Button addReview;

    private DatabaseReference mRef;
    private FirebaseUser mUser;

    private String userToBeReviewed;
    private List<Review> reviews;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_new_review, container, false);

        if (getArguments() != null) {
            userToBeReviewed = getArguments().getString("userId");
            Log.d(TAG, "USER TO BE REVIEWED: " + userToBeReviewed);
        }

        onInit();
        addReview.setOnClickListener(v -> saveReview());

        return view;
    }

    private void onInit() {
        review = view.findViewById(R.id.text);
        addReview = view.findViewById(R.id.addReview);
        error = view.findViewById(R.id.error);

        mRef = FirebaseDatabase.getInstance().getReference().child(DatabaseConstants.REVIEWS_DATABASE);
        mUser = FirebaseAuth.getInstance().getCurrentUser();


    }

    private void saveReview() {
        String reviewText = review.getText().toString();
        if (!isReviewValid(reviewText)) {
            error.setVisibility(View.VISIBLE);
        } else {
            Review review = new Review(mUser.getUid(), reviewText, getCurrentDate());
            mRef.child(userToBeReviewed).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    reviews = (List<Review>) dataSnapshot.getValue();
                    if (reviews == null || reviews.size() <= 0) {
                        reviews = new ArrayList<>();
                        Log.d(TAG, "saveReview: " + reviews.size());
                    }
                    reviews.add(review);
                    Log.d(TAG, "saveReview: " + reviews.size());
                    mRef.child(userToBeReviewed).setValue(reviews);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            openUserProfile(userToBeReviewed);
        }
    }

    private boolean isReviewValid(String reviewText) {
        if (TextUtils.isEmpty(reviewText)) {
            return false;
        } else {
            return true;
        }
    }

    private void openUserProfile(String userId) {
        Bundle bundle = new Bundle();
        bundle.putString("userId", userId);
        ProfileFragment profileFragment = new ProfileFragment();
        profileFragment.setArguments(bundle);
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, profileFragment)
                .addToBackStack(null)
                .commit();
    }

    private String getCurrentDate() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime currentDate = LocalDateTime.now();
        return dateTimeFormatter.format(currentDate);
    }

}
