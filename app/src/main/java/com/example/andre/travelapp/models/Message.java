package com.example.andre.travelapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Message implements Parcelable {

    private String uid;
    private String name;
    private String message;
    private String date;

    public Message() {
    }

    public Message(String uid, String name, String message, String date) {
        this.uid = uid;
        this.name = name;
        this.message = message;
        this.date = date;
    }

    protected Message(Parcel in) {
        uid = in.readString();
        name = in.readString();
        message = in.readString();
        date = in.readString();
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uid);
        dest.writeString(name);
        dest.writeString(message);
        dest.writeString(date);
    }
}