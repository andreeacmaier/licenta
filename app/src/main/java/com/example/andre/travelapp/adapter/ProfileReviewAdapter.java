package com.example.andre.travelapp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.andre.travelapp.R;
import com.example.andre.travelapp.constants.DatabaseConstants;
import com.example.andre.travelapp.models.Review;
import com.example.andre.travelapp.models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import static androidx.constraintlayout.motion.widget.MotionScene.TAG;

public class ProfileReviewAdapter extends RecyclerView.Adapter<ProfileReviewAdapter.ViewHolder> {

    private Context context;
    private List<Review> reviews;
    private FragmentManager fragmentManager;

    public ProfileReviewAdapter() {
    }

    public ProfileReviewAdapter(Context context, List<Review> reviews, FragmentManager fragmentManager) {
        this.context = context;
        this.reviews = reviews;
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public ProfileReviewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.review_item, parent, false);
        return new ProfileReviewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileReviewAdapter.ViewHolder holder, int position) {
        Review review = reviews.get(position);

        Log.d(TAG, "ProfileReviewAdapter: " + review.getReview() + "   " +review.getReviewerUID());
        FirebaseDatabase.getInstance().getReference().child(DatabaseConstants.USERS_DATABASE)
                .child(review.getReviewerUID()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                String imageUri = user.getProfilePicture();
                String name = user.getUsername();
                if (imageUri != null) {
                    Glide.with(context).
                            load(imageUri)
                            .placeholder(R.drawable.explorer)
                            .into(holder.profileImage);
                } else {
                    Log.d(TAG, "ERROR LOADING IMAGE URI: ");
                }
                holder.messageName.setText(name + ": " + review.getReview());
                holder.date.setText(review.getInsertedTimestamp());
                Log.d(TAG, "ProfileReviewAdapter: " + user.getUsername());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public int getItemCount() {
        if (reviews == null) {
            return 0;
        } else {
            return reviews.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView profileImage;
        TextView messageName, date;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            profileImage = itemView.findViewById(R.id.profile_image);
            messageName = itemView.findViewById(R.id.user_message);
            date = itemView.findViewById(R.id.date);
        }
    }
}
