package com.example.andre.travelapp.ui.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.andre.travelapp.R;
import com.example.andre.travelapp.adapter.DashboardAdapter;
import com.example.andre.travelapp.constants.ActivityConstants;
import com.example.andre.travelapp.constants.DatabaseConstants;
import com.example.andre.travelapp.models.Trip;
import com.example.andre.travelapp.scheduler.Scheduler;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class DashboardFragment extends Fragment implements LocationListener {

    private final static String TAG = "Log TAG ";

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private FirebaseDatabase mDatabase;
    private DatabaseReference mRef;

    //RecyclerView utils
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private DashboardAdapter dashboardAdapter;

    private View view;

    private int selectedItemPosition = 0;
    private List<Trip> dbTrips;
    private Scheduler scheduler = new Scheduler();

    private LocationManager locationManager;
    private String lat;
    private String lng;
    private Location currentLocation;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        scheduler.deleteFinishedTrips();
        onInit();

        return view;
    }

    private void onInit() {
        Spinner dropdown = view.findViewById(R.id.dropdown);

        recyclerView = view.findViewById(R.id.linear_recyclerview);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        dashboardAdapter = new DashboardAdapter();
        recyclerView.setAdapter(dashboardAdapter);

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance();
        mRef = mDatabase.getReference();

        startGetLocation();

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item,
                ActivityConstants.DROPDOWN_OPTIONS);
        spinnerAdapter.setDropDownViewResource(R.layout.spinner_item);
        dropdown.setAdapter(spinnerAdapter);
        dropdown.setOnItemSelectedListener(dropdownListener());

        dbTrips = new ArrayList<>();
    }

    private void startGetLocation() {
        Log.d(TAG, "startGetLocation()");
        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                getCurrentLocation();
                handler.postDelayed(this, 10 * 1000);
            }
        };
    }

    private void updateRecyclerByDropdownSelectedItem() {
        if (selectedItemPosition == 0) {
            Collections.sort(dbTrips, compareByInsertTimestamp.reversed());
        } else if (selectedItemPosition == 1) {
            Collections.sort(dbTrips, compareByStartDate);
        } else if (selectedItemPosition == 2) {
            Collections.sort(dbTrips, compareByStartDate.reversed());
        } else if (selectedItemPosition == 3) {
            getCurrentLocation();
            if (lat != null && lng != null) {
                Collections.sort(dbTrips, compareByLocation);
            }
        }
    }

    private AdapterView.OnItemSelectedListener dropdownListener() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedItemPosition = position;
                getAllTrips();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
    }

    private void getAllTrips() {
        DatabaseReference tripsRef = mRef.child(DatabaseConstants.TRIPS_DATABASE);

        tripsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dbTrips.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Trip trip = snapshot.getValue(Trip.class);
                    if (LocalDateTime.now().compareTo(formatDate(trip.getStartDate())) < 0) {
                        dbTrips.add(trip);
                    }
                }
                Log.d(TAG, "Active trips at the moment: " + dbTrips.size());
                updateRecyclerByDropdownSelectedItem();

                if (mUser.isEmailVerified()) {
                    dashboardAdapter = new DashboardAdapter(getContext(), dbTrips, mUser.getUid(), mRef);
                    dashboardAdapter.notifyDataSetChanged();
                    recyclerView.setAdapter(dashboardAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private Comparator<Trip> compareByLocation = (Trip t1, Trip t2) -> {
        Location firstTripLocation = new Location("");
        firstTripLocation.setLatitude(t1.getLocations().get(0).getLatLng().getLat());
        firstTripLocation.setLongitude(t1.getLocations().get(0).getLatLng().getLng());
        Float firstDistance = currentLocation.distanceTo(firstTripLocation);

        Location secondTripLocation = new Location("");
        secondTripLocation.setLatitude(t2.getLocations().get(0).getLatLng().getLat());
        secondTripLocation.setLongitude(t2.getLocations().get(0).getLatLng().getLng());
        Float secondDistance = currentLocation.distanceTo(secondTripLocation);

        return firstDistance.compareTo(secondDistance);
    };

    private Comparator<Trip> compareByStartDate = (Trip t1, Trip t2) -> {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime ldt1 = LocalDateTime.parse(t1.getStartDate(), formatter);
        LocalDateTime ldt2 = LocalDateTime.parse(t2.getStartDate(), formatter);
        return ldt1.compareTo(ldt2);
    };

    private Comparator<Trip> compareByInsertTimestamp = (Trip t1, Trip t2) -> {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-dd");
        LocalDate ldt1 = LocalDate.parse(t1.getInsertedTimestamp(), formatter);
        LocalDate ldt2 = LocalDate.parse(t2.getInsertedTimestamp(), formatter);
        return ldt1.compareTo(ldt2);
    };

    private LocalDateTime formatDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return LocalDateTime.parse(date, formatter);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        locationManager = (LocationManager) requireActivity().getSystemService(Context.LOCATION_SERVICE);
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.i(TAG, "onResume");
        getCurrentLocation();
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.i(TAG, "onPause");
        locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;

        lat = String.valueOf(location.getLatitude());
        lng = String.valueOf(location.getLongitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(requireActivity(), "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }

    private void getCurrentLocation() {
        try {
            if (isAdded()) {
                locationManager = (LocationManager) requireActivity().getSystemService(Context.LOCATION_SERVICE);
                if (requireActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        requireActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET}, 1);
                    return;
                } else {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, this);
                }
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }
}
