package com.example.andre.travelapp.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.andre.travelapp.R;
import com.example.andre.travelapp.adapter.InboxAdapter;
import com.example.andre.travelapp.constants.DatabaseConstants;
import com.example.andre.travelapp.models.Chat;
import com.example.andre.travelapp.models.Message;
import com.example.andre.travelapp.models.Trip;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class InboxFragment extends Fragment {

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private FirebaseDatabase mDatabase;
    private DatabaseReference mRef;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private InboxAdapter inboxAdapter;

    private View view;
    private String currentUserUid;
    private Map<String, Trip> tripsMap;
    private List<Chat> chats;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_inbox, container, false);

        onInit();
        return view;
    }

    private void onInit() {
        recyclerView = view.findViewById(R.id.inbox_recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        inboxAdapter = new InboxAdapter();
        recyclerView.setAdapter(inboxAdapter);

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance();
        mRef = mDatabase.getReference();
        currentUserUid = mUser.getUid();

        tripsMap = new HashMap<>();

        chats = new ArrayList<>();

        displayChats2();
    }

    private void displayChats2() {
        DatabaseReference chatsRef = mRef.child(DatabaseConstants.CHATS_DATABASE);
        chatsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    chats.clear();
                    String snapshotKey = snapshot.getKey();
                    List<String> attendigUsersList = (List<String>) snapshot
                            .child(DatabaseConstants.CHAT_ATTENDIG_USERS)
                            .getValue();
                    String title = snapshot.child(DatabaseConstants.CHAT_TITLE).getValue(String.class);
                    if (attendigUsersList != null && attendigUsersList.contains(currentUserUid)) {
                        Query messageSnapshot = chatsRef.child(snapshotKey).child(DatabaseConstants.CHAT_MESSAGES)
                                .orderByKey().limitToLast(1);
                        Chat chat = new Chat();
                        chat.setTitle(title);
                        chat.setChatId(snapshotKey);
                        messageSnapshot.addValueEventListener(lastMessageListener2(chat));
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private ValueEventListener lastMessageListener2(Chat chat) {
        return new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Message message = snapshot.getValue(Message.class);
                    chat.setMessage(message);
                    chats.add(chat);
                }

                Collections.sort(chats, lastMessageComparator.reversed());
                if (isAdded()) {
                    inboxAdapter = new InboxAdapter(getContext(), requireActivity().getSupportFragmentManager(), chats);
                    recyclerView.setAdapter(inboxAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
    }

    private Comparator<Chat> lastMessageComparator = (Chat t1, Chat t2) -> {
        String date1 = t1.getMessage().getDate();
        String date2 = t2.getMessage().getDate();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime ldt1 = LocalDateTime.parse(date1, formatter);
        LocalDateTime ldt2 = LocalDateTime.parse(date2, formatter);
        return ldt1.compareTo(ldt2);
    };

}
