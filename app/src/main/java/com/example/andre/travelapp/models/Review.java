package com.example.andre.travelapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

public class Review implements Parcelable {

    private String reviewerUID;
    private String review;
    private String insertedTimestamp;

    public Review() {
    }

    public Review(String reviewerUID, String review, String insertedTimestamp) {
        this.reviewerUID = reviewerUID;
        this.review = review;
        this.insertedTimestamp = insertedTimestamp;
    }

    public String getReviewerUID() {
        return reviewerUID;
    }

    public void setReviewerUID(String reviewerUID) {
        this.reviewerUID = reviewerUID;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getInsertedTimestamp() {
        return insertedTimestamp;
    }

    public void setInsertedTimestamp(String insertedTimestamp) {
        this.insertedTimestamp = insertedTimestamp;
    }

    protected Review(Parcel in) {
        reviewerUID = in.readString();
        review = in.readString();
        insertedTimestamp = in.readString();
    }

    public static final Creator<Review> CREATOR = new Creator<Review>() {
        @Override
        public Review createFromParcel(Parcel in) {
            return new Review(in);
        }

        @Override
        public Review[] newArray(int size) {
            return new Review[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(reviewerUID);
        dest.writeString(review);
        dest.writeString(insertedTimestamp);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Review review1 = (Review) o;
        return Objects.equals(reviewerUID, review1.reviewerUID) &&
                Objects.equals(review, review1.review) &&
                Objects.equals(insertedTimestamp, review1.insertedTimestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reviewerUID, review, insertedTimestamp);
    }
}
