package com.example.andre.travelapp.ui.fragments;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.andre.travelapp.R;
import com.example.andre.travelapp.constants.ActivityConstants;
import com.example.andre.travelapp.constants.DatabaseConstants;
import com.example.andre.travelapp.models.LocationModel;
import com.example.andre.travelapp.models.Trip;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.PhotoMetadata;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPhotoRequest;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.android.volley.VolleyLog.TAG;

public class MyTripsFragment extends Fragment {

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private FirebaseDatabase mDatabase;
    private DatabaseReference mRef;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FirebaseRecyclerAdapter adapter;

    private View view;
    private String currentUserUid;
    private PlacesClient placesClient;
    List<Trip> trips;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_mytrip, container, false);

        onInit();

        return view;
    }

    private void onInit() {
        recyclerView = view.findViewById(R.id.linear_recyclerview);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance();
        mRef = mDatabase.getReference();
        currentUserUid = mUser.getUid();

        Places.initialize(getContext(), DatabaseConstants.GOOGLE_API_KEY);
        placesClient = Places.createClient(getContext());
        trips = new ArrayList<>();

        getCurrentUserTrips();
    }

    private void getCurrentUserTrips() {
        Query tripsRef = mRef.child(DatabaseConstants.TRIPS_DATABASE);
        DatabaseReference currentUserRef = mRef.child(DatabaseConstants.USERS_DATABASE).child(currentUserUid);

        tripsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Trip trip = snapshot.getValue(Trip.class);
                    trips.add(trip);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        FirebaseRecyclerOptions<String> options1 =
                new FirebaseRecyclerOptions.Builder<String>()
                        .setQuery(currentUserRef.child(DatabaseConstants.USERS_MY_TRIPS)
                                .orderByChild(DatabaseConstants.TRIP_START_DATE).limitToLast(100),
                                snapshot -> snapshot.getValue(String.class))
                        .build();


        setAdapter(options1);
    }

    private void setAdapter(FirebaseRecyclerOptions<String> options) {
        adapter = new FirebaseRecyclerAdapter<String, MyTripsViewHolder>(options) {
            @NonNull
            @Override
            public MyTripsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.mytrip_item, parent, false);
                return new MyTripsFragment.MyTripsViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull MyTripsViewHolder myTripsViewHolder, int i, @NonNull String s) {
                for (Trip trip : trips) {
                    if (trip.getTripId().equals(s)) {
                        myTripsViewHolder.titleTextView.setText(trip.getTitle());
                        myTripsViewHolder.startDateTextView.setText("Departure: " + trip.getStartDate());
                        ArrayList<Bitmap> imageList = getPicture(trip, myTripsViewHolder.imageView
                                , myTripsViewHolder.progressBar);

                        if (trip.getUserAdminUid().equals(currentUserUid)) {
                            myTripsViewHolder.updateBtn.setVisibility(View.VISIBLE);
                        } else {
                            myTripsViewHolder.updateBtn.setVisibility(View.INVISIBLE);
                        }

                        myTripsViewHolder.itemView.setOnClickListener(v -> {
                            SingleTripFragment singleTripFragment = new SingleTripFragment();
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("trip", trip);
                            bundle.putParcelableArrayList("imageList", imageList);
                            singleTripFragment.setArguments(bundle);
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container, singleTripFragment)
                                    .addToBackStack(null).commit();
                        });

                        myTripsViewHolder.updateBtn.setOnClickListener(v -> {
                            NewTripFragment newTripFragment = new NewTripFragment();
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("trip", trip);
                            bundle.putString(ActivityConstants.PREVIOUS_FRAGMENT_KEY, ActivityConstants.MY_TRIPS_FRAGMENT);
                            newTripFragment.setArguments(bundle);
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container, newTripFragment)
                                    .addToBackStack(null).commit();
                        });
                    }
                }
            }
        };
        recyclerView.setAdapter(adapter);
    }


    /**
     * Adapter listening
     */
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    private ArrayList<Bitmap> getPicture(Trip trip, ImageView imageView, ProgressBar progressBar) {
        Random random = new Random();
        List<LocationModel> locations = trip.getLocations();
        String placeId;
        List<Place.Field> placeFields;
        FetchPlaceRequest request;
        ArrayList<Bitmap> imageList = new ArrayList<>();

        for (LocationModel location : locations) {
            placeId = location.getId();
            placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.PHOTO_METADATAS);
            request = FetchPlaceRequest.builder(placeId, placeFields).build();

            placesClient.fetchPlace(request).addOnSuccessListener((response) -> {
                Place place = response.getPlace();
                Log.i(TAG, "Place found: " + place.getName());
                PhotoMetadata photoMetadata = place.getPhotoMetadatas().get(0);

                FetchPhotoRequest photoRequest = FetchPhotoRequest.builder(photoMetadata)
                        .setMaxWidth(500)
                        .setMaxHeight(300)
                        .build();
                placesClient.fetchPhoto(photoRequest).addOnSuccessListener((fetchPhotoResponse) -> {
                    Bitmap bitmap = fetchPhotoResponse.getBitmap();
                    boolean added = imageList.add(bitmap);
                    Log.d(TAG, "getPicture: " + added + "  size = " + imageList.size());
                    setImage(bitmap, imageView, progressBar);
                    //imageView.setImageBitmap(bitmap);
                }).addOnFailureListener((exception) -> {
                    if (exception instanceof ApiException) {
                        ApiException apiException = (ApiException) exception;
                        Log.e(TAG, "Place not found: " + exception.getMessage());
                    }
                });

            }).addOnFailureListener((exception) -> {
                if (exception instanceof ApiException) {
                    ApiException apiException = (ApiException) exception;
                    int statusCode = apiException.getStatusCode();
                    Log.e(TAG, "Place not found: " + exception.getMessage());
                }
            });
        }

        if (imageList.size() > 0) {
            int index = random.nextInt(imageList.size() - 1);
            Log.d(TAG, "getPicture: " + index);
            imageView.setImageBitmap(imageList.get(index));
        }

        return imageList;

    }

    public class MyTripsViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView startDateTextView;
        TextView titleTextView;
        Button updateBtn;
        ProgressBar progressBar;

        public MyTripsViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image_view);
            titleTextView = itemView.findViewById(R.id.list_title);
            startDateTextView = itemView.findViewById(R.id.list_startDate);
            updateBtn = itemView.findViewById(R.id.update_btn);
            progressBar = itemView.findViewById(R.id.progress);
        }
    }

    private void setImage (Bitmap imageBitmap, ImageView imageView, ProgressBar progressBar){
        if (isAdded()){
            Glide.with(getContext())
                    .load(imageBitmap)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(imageView);
        }
    }
}
