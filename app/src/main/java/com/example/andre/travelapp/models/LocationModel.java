package com.example.andre.travelapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class LocationModel implements Parcelable {

    private LatLong latLng;
    private String address;
    private String name;
    private String id;

    public LocationModel() {
    }


    protected LocationModel(Parcel in) {
        latLng = in.readParcelable(LatLong.class.getClassLoader());
        address = in.readString();
        name = in.readString();
        id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(latLng, flags);
        dest.writeString(address);
        dest.writeString(name);
        dest.writeString(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LocationModel> CREATOR = new Creator<LocationModel>() {
        @Override
        public LocationModel createFromParcel(Parcel in) {
            return new LocationModel(in);
        }

        @Override
        public LocationModel[] newArray(int size) {
            return new LocationModel[size];
        }
    };

    public LatLong getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLong latLng) {
        this.latLng = latLng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    @Override
    public String toString() {
        return "LocationModel{" +
                "latLng=" + latLng +
                ", address='" + address + '\'' +
                ", name='" + name + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
