package com.example.andre.travelapp.ui.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.andre.travelapp.R;
import com.example.andre.travelapp.constants.DatabaseConstants;
import com.example.andre.travelapp.models.User;
import com.example.andre.travelapp.utils.ImageUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import static android.app.Activity.RESULT_OK;

public class UpdateProfileFragment extends Fragment {

    private static final String TAG = "TAG";
    private View view;

    private ImageView profilePicture;
    private EditText nameView, descriptionView;
    private TextView changeProfilePictureView;
    private Button updateBtn;

    private DatabaseReference mRef;
    private FirebaseUser mUser;
    private DatabaseReference userRef;

    private Uri imageUri;
    private ImageUtils imageUtils;
    private boolean imageWasChanged = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_update_profile, container, false);

        onInit();
        changeProfilePictureView.setOnClickListener(v -> openGallery());
        updateBtn.setOnClickListener(v -> updateProfile());
        return view;
    }

    private void onInit() {
        profilePicture = view.findViewById(R.id.profile_image);
        nameView = view.findViewById(R.id.name);
        descriptionView = view.findViewById(R.id.description);
        changeProfilePictureView = view.findViewById(R.id.changeProfilePicture);
        updateBtn = view.findViewById(R.id.update_btn);

        mRef = FirebaseDatabase.getInstance().getReference();
        mUser = FirebaseAuth.getInstance().getCurrentUser();

        imageUtils = new ImageUtils(getContext());

        completeFieldsWithCurrentDetails();
    }

    private void completeFieldsWithCurrentDetails() {
        userRef = mRef.child(DatabaseConstants.USERS_DATABASE).child(mUser.getUid());
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                nameView.setText(user.getUsername());
                if (dataSnapshot.hasChild(DatabaseConstants.USERS_DESCRIPTION)) {
                    descriptionView.setText(user.getDescription());
                }
                setProfilePicture(user.getProfilePicture());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setProfilePicture(String imageUri) {
        Glide.with(getContext()).
                load(imageUri)
                .placeholder(R.drawable.explorer)
                .into(profilePicture);
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        UpdateProfileFragment.this.startActivityForResult(intent, 5);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 5 && resultCode == RESULT_OK && data != null) {
            imageUri = data.getData();
            Log.d(TAG, "onActivityResult: " + imageUri);
            profilePicture.setImageURI(imageUri);
            imageWasChanged = true;
        }
    }

    private void updateProfile() {
        String newName = nameView.getText().toString();
        String newDescription = descriptionView.getText().toString();

        if (TextUtils.isEmpty(newName)) {
            Toast.makeText(getContext(), "Name cannot be empty.", Toast.LENGTH_SHORT).show();
        } else {
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    user.setUsername(newName);
                    user.setDescription(newDescription);
                    if (imageWasChanged) {
                        imageUtils.uploadImage(imageUri, userRef, mUser.getUid());
                    }
                    userRef.setValue(user);

                    returnToProfileFragment();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            imageUtils.uploadImage(imageUri, userRef, mUser.getUid());
        }
    }

    private void returnToProfileFragment() {
        ProfileFragment profileFragment = new ProfileFragment();
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, profileFragment)
                .addToBackStack(null).commit();
    }


}
