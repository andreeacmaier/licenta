package com.example.andre.travelapp.models;

public class GroupMember {

    private String name;
    private String imgUri;
    private String userUid;

    public GroupMember(String name, String imgUri, String userUid) {
        this.name = name;
        this.imgUri = imgUri;
        this.userUid = userUid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUri() {
        return imgUri;
    }

    public void setImgUri(String imgUri) {
        this.imgUri = imgUri;
    }

    public String getUserUid() {
        return userUid;
    }

    public void setUserUid(String userUid) {
        this.userUid = userUid;
    }
}
