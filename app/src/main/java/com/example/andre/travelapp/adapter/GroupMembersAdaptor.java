package com.example.andre.travelapp.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.andre.travelapp.R;
import com.example.andre.travelapp.models.GroupMember;
import com.example.andre.travelapp.ui.fragments.ProfileFragment;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

public class GroupMembersAdaptor extends RecyclerView.Adapter<GroupMembersAdaptor.ViewHolder> {

    private Context context;
    private List<GroupMember> userList;
    private FragmentManager fragmentManager;

    public GroupMembersAdaptor() {
    }

    @NonNull
    @Override
    public GroupMembersAdaptor.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.group_member_item, parent, false);
        return new GroupMembersAdaptor.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupMembersAdaptor.ViewHolder holder, int position) {
        String imageUri = userList.get(position).getImgUri();
        String name = userList.get(position).getName();

        if (position == 0) {
            holder.name.setText("ADMIN - " + name);
        } else
            holder.name.setText(name);
        if (imageUri != null) {
            Glide.with(context).
                    load(imageUri)
                    .placeholder(R.drawable.explorer)
                    .into(holder.profileImage);
        }

        holder.itemView.setOnClickListener(v -> openUserProfile(userList.get(position).getUserUid()));
    }

    public void openUserProfile(String userId){
        Bundle bundle = new Bundle();
        bundle.putString("userId", userId);
        ProfileFragment profileFragment = new ProfileFragment();
        profileFragment.setArguments(bundle);
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, profileFragment)
                .addToBackStack(null)
                .commit();
    }

    public GroupMembersAdaptor(Context context, List<GroupMember> userList, FragmentManager fragmentManager) {
        this.context = context;
        this.userList = userList;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public int getItemCount() {
        if (userList == null) {
            return 0;
        } else {
            return userList.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView profileImage;
        TextView name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            profileImage = itemView.findViewById(R.id.profile_image);
            name = itemView.findViewById(R.id.name);
        }
    }
}
