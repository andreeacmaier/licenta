package com.example.andre.travelapp.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.andre.travelapp.R;
import com.example.andre.travelapp.models.Chat;
import com.example.andre.travelapp.models.Message;
import com.example.andre.travelapp.ui.fragments.ChatFragment;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;


public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.ViewHolder> {

    private Context context;
    private FragmentManager fragmentManager;
    private List<Chat> chatList;

    public InboxAdapter() {
    }

    public InboxAdapter(Context context, FragmentManager fragmentManager, List<Chat> chats) {
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.chatList = chats;
    }

    @NonNull
    @Override
    public InboxAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.inbox_item, parent, false);
        return new InboxAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InboxAdapter.ViewHolder holder, int position) {
        String title = chatList.get(position).getTitle();
        Message message = chatList.get(position).getMessage();

        String lastMessage = message.getName() + ": " +
                message.getMessage() + " \n" +
                message.getDate() + " ";

        holder.chatTitleTextView.setText(title);
        holder.username.setText(lastMessage);

        holder.itemView.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putParcelable("chat", chatList.get(position));
            ChatFragment chatFragment = new ChatFragment();
            chatFragment.setArguments(bundle);

            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, chatFragment)
                    .addToBackStack(null).commit();
        });
    }

    @Override
    public int getItemCount() {
        if (chatList != null) {
            return chatList.size();
        } else {
            return 0;
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView chatTitleTextView;
        TextView username;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            chatTitleTextView = itemView.findViewById(R.id.chat_title);
            username = itemView.findViewById(R.id.username_last_message);
        }
    }
}
