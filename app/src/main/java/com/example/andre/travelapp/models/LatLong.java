package com.example.andre.travelapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class LatLong implements Parcelable {

    private double lat;
    private double lng;

    public LatLong() {
    }

    public LatLong(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    protected LatLong(Parcel in) {
        lat = in.readDouble();
        lng = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(lat);
        dest.writeDouble(lng);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LatLong> CREATOR = new Creator<LatLong>() {
        @Override
        public LatLong createFromParcel(Parcel in) {
            return new LatLong(in);
        }

        @Override
        public LatLong[] newArray(int size) {
            return new LatLong[size];
        }
    };

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "LatLong{" +
                "lat=" + lat +
                ", lng=" + lng +
                '}';
    }
}
