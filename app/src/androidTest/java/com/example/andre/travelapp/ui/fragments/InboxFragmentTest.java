package com.example.andre.travelapp.ui.fragments;

import android.content.Intent;

import com.example.andre.travelapp.R;
import com.example.andre.travelapp.ui.activities.LoginActivity;
import com.example.andre.travelapp.ui.activities.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class InboxFragmentTest {

    private static final String EMAIL = "am@mail.com";
    private static final String PASSWORD = "123456";
    private static final String ACTIVE_CHATS_TEXT = "ACTIVE CHATS";
    private static final String MESSAGE = "This message is for testing purposes.";

    @Rule
    public ActivityTestRule<LoginActivity> loginActivityRule
            = new ActivityTestRule<>(LoginActivity.class, true, false);

    @Rule
    public ActivityTestRule<MainActivity> mainActivityRule
            = new ActivityTestRule<>(MainActivity.class, true, false);

    @Before
    public void login() {
        Intent intent = new Intent();
        loginActivityRule.launchActivity(intent);

        onView(withId(R.id.emailField)).perform(typeText(EMAIL), closeSoftKeyboard());
        onView(withId(R.id.passwordField)).perform(typeText(PASSWORD), closeSoftKeyboard());
        onView(withId(R.id.signIn_button)).perform(click());
    }

    @Test
    public void testInboxScreenIsDisplayed() throws InterruptedException {
        Thread.sleep(1000);
        onView(withId(R.id.nav_inbox)).perform(click());
        onView(withId(R.id.title)).check(matches(withText(ACTIVE_CHATS_TEXT)));
    }

    @Test
    public void testChatGroupIsOpen() throws InterruptedException {
        Thread.sleep(1000);
        // when
        onView(withId(R.id.nav_inbox)).perform(click());
        Thread.sleep(1000);
        onView(withId(R.id.inbox_recycler_view)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        // then
        onView(withId(R.id.send_message_btn)).check(matches(isDisplayed()));
    }

    @Test
    public void testSendMessage() throws InterruptedException {
        Thread.sleep(1000);
        // when
        onView(withId(R.id.nav_inbox)).perform(click());
        Thread.sleep(1000);
        onView(withId(R.id.inbox_recycler_view)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        onView(withId(R.id.message_input)).perform(typeText(MESSAGE), closeSoftKeyboard());
        onView(withId(R.id.send_message_btn)).perform(click());
        // then
        Thread.sleep(1000);
        onView(withText(MESSAGE)).check(matches(isDisplayed()));

    }

    @After
    public void logout() {
        onView(withId(R.id.nav_profile)).perform(click());
        onView(withId(R.id.logout_Btn)).perform(click());
    }

}