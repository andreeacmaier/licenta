package com.example.andre.travelapp.ui.fragments;

import android.content.Intent;

import com.example.andre.travelapp.R;
import com.example.andre.travelapp.ui.activities.LoginActivity;
import com.example.andre.travelapp.ui.activities.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class DashboardFragmentTest {

    private static final String EMAIL = "am@mail.com";
    private static final String PASSWORD = "123456";
    private static final String WELCOME_TEXT = "Take a look and find your next adventure.";

    @Rule
    public ActivityTestRule<LoginActivity> loginActivityRule
            = new ActivityTestRule<>(LoginActivity.class, true, false);

    @Rule
    public ActivityTestRule<MainActivity> mainActivityRule
            = new ActivityTestRule<>(MainActivity.class, true, false);

    @Before
    public void login() {
        Intent intent = new Intent();
        loginActivityRule.launchActivity(intent);

        onView(withId(R.id.emailField)).perform(typeText(EMAIL), closeSoftKeyboard());
        onView(withId(R.id.passwordField)).perform(typeText(PASSWORD), closeSoftKeyboard());
        onView(withId(R.id.signIn_button)).perform(click());
    }

    @Test
    public void testHomeScreenIsDisplayed() throws InterruptedException {
        Thread.sleep(1000);
        onView(withId(R.id.textView)).check(matches(withText(WELCOME_TEXT)));
        onView(withId(R.id.dropdown)).check(matches(isDisplayed()));
    }

    @After
    public void logout() {
        onView(withId(R.id.nav_profile)).perform(click());
        onView(withId(R.id.logout_Btn)).perform(click());
    }

}