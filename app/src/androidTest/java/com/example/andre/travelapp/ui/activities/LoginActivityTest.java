package com.example.andre.travelapp.ui.activities;

import com.example.andre.travelapp.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.espresso.intent.Intents;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;


@LargeTest
public class LoginActivityTest {

    private static final String EMAIL = "am@mail.com";
    private static final String PASSWORD = "123456";

    @Rule
    public ActivityTestRule<LoginActivity> loginActivityRule
            = new ActivityTestRule<>(LoginActivity.class);

    @Before
    public void setUp() {
        Intents.init();
    }

    @Test
    public void testLoginWithValidCredentialsAndThenLogout() throws InterruptedException {
        onView(withId(R.id.emailField)).perform(typeText(EMAIL), closeSoftKeyboard());
        onView(withId(R.id.passwordField)).perform(typeText(PASSWORD), closeSoftKeyboard());
        onView(withId(R.id.signIn_button)).perform(click());
        Thread.sleep(1000);
        onView(withId(R.id.nav_profile)).perform(click());
        onView(withId(R.id.logout_Btn)).perform(click());

        //check if LoginActivity is displayed after logout
        onView(withId(R.id.signIn_button)).check(matches(isDisplayed()));
    }

    @After
    public void release() {
        Intents.release();
    }
}